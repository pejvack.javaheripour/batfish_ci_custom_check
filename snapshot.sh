#!/bin/bash

## Cleanup output directory!
rm -R /vagrant/batfish_ci_custom_check/output/*

## Download the configs!
cd /vagrant/batfish_ci_custom_check/download/
git clone https://gitlab.com/pejvack.javaheripour/network_device_config_files.git
mv network_device_config_files/snapshot/ /vagrant/batfish_ci_custom_check/

## cleanup clocal repository!
rm -R /vagrant/batfish_ci_custom_check/download/network_device_config_files

